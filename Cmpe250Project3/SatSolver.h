#ifndef SATSOLVER_H_
#define SATSOLVER_H_

#include <string>
#include <fstream>
#include <iostream>
#include <stack>
#include <vector>
#include <forward_list>
#include <queue>
#include <list>

using AdjList = std::vector<std::list<int>>;
using EdgeList = std::vector<std::pair<int, int>>;


class SatSolver{
    
public:
    int variables = 0;//number of variables
    int size =0 ;
    int clauses = 0;
    //index number that is given in scc method
    int countTurn = 1;
    
    //adj structure for the part1
    std::vector<std::list<int>> AdjL ;
    
    // edges from the file
    std::vector<std::pair<int, int>> EdgeList ;
    
    //Main Stack for Sccs
    std::stack <int> SccStack ;
    
    //for storing Sccs
    std::stack<int>reverseStack;
    
    //structure that goes to file
    std::vector<std::stack<int>> part2Sccs;
    
    //strong components
    std::vector<std::vector<int>> part3Sccs;
    
    // for part 3 ,individual scc
    std::vector<int> part3Comp ;
    
    //tarjan's requirement
    std::vector<int> lowlink;
    
    //also tarjan's requirement
    std::vector<int> index;
    
    // tarjan's requirement
    std::vector<bool> isInStack ;
    
    // values of the variables
    std::vector<int> part3Values;
    
    //state of their discovery
    std::vector<int> part3SccValues;
    
    //builds the adj from edgeset
    void Build(int num_vertices,const ::EdgeList &edges);
    //for negative vertices
    int indexify(int n);
    //prints the adjlist to the file
    void PrintAdjList(std::string filename,const ::AdjList &adjlist,int vertices);
    
    //finds the strongly connected components
    void Scc(int node);
    // Reads the input file
    SatSolver(std::string filename);
    
    // Outputs the adjacency list to the file
    void Part1(std::string filename);
    
    // Outputs the strongly connected components to the file
    void Part2(std::string filename);
    
    // Outputs the assignments to the file
    void Part3(std::string filename);
    
};



#endif
