#include "SatSolver.h"
#include <iostream>
#include <fstream>
#include <queue>
#include <vector>
#include <algorithm>
#include <forward_list>

using namespace std ;

/*
 Creates a Adj from the grap using a vector of list
 */
void SatSolver::Build(int num_vertices,const ::EdgeList &edges){
    AdjL.resize(num_vertices * 2);
    for(auto &edge: edges){
        int edgeTemp = edge.first;
        if(edgeTemp < 0){
            edgeTemp +=num_vertices;
        }else{
            edgeTemp +=(num_vertices-1);
        }
        AdjL[edgeTemp].push_back(edge.second);
    }
}

/*
 takes a filename,Adj and number of variables then prints adj to the file,for part1
 */
void SatSolver::PrintAdjList(std::string filename,const AdjList &adjlist,int num_vertices){
    std::ofstream fout(filename);
    if(!fout.is_open()){
        return;
    }
    int nodelabel = 0;
    for(auto& nodes_list : adjlist){
        if(nodelabel< num_vertices){//edge number corrections
            fout << nodelabel++-num_vertices<< " ";
        }else{
            fout << nodelabel++-num_vertices +1<< " ";
        }
        for(int node : nodes_list){
            fout<<node<<" ";
        }
        fout<<std::endl;
    }
}
/*
 Gives the index of an input negative vertex
 */
int SatSolver::indexify(int n){
    if(n < 0){
        return n + variables ;
    }else{
        return n + variables -1 ;
    }
}

/*
 Takes the node and processes it according to the Tarjan's Strongly connected components
 algorithm.
 */
void SatSolver::Scc(int node){
    int process =indexify(node);
    lowlink[process] = index[process] = countTurn++ ;
    //after indexing and lowlinking put the node to the stack
    SccStack.push(node);
    isInStack[process] = true ;
    if(!AdjL[process].empty()){
        auto NodeLoc = AdjL[process];
        int process2 = process ;
        //process the neighbour nodes
        for(int nodes : NodeLoc){
            process2 = indexify(nodes);
            //this if else is the key part of the stack invariants property
            if(index[process2] == -1){
                Scc(nodes);
                lowlink[process] = min(lowlink[process],lowlink[process2]);
            }else if (isInStack[process2]){
                lowlink[process] = min(lowlink[process],index[process2]);
            }
        }
    }
    int out =0;
    // if lowlink and index is equal that means no more parent node link and it is root
    if(lowlink[process] == index[process]){
        //so print the stack to storing until root is fount
        while(SccStack.top() != node){
            out = (int) SccStack.top();
            isInStack[indexify(out)] =false;
            SccStack.pop();
            reverseStack.push(out);
            part3Comp.push_back(out);
            
        }
        //then print the root
        out = (int) SccStack.top();
        isInStack[indexify(out)] =false;
        SccStack.pop();
        reverseStack.push(out);
        part3Comp.push_back(out);
        part2Sccs.push_back(reverseStack);
        part3Sccs.push_back(part3Comp);
        while (!reverseStack.empty()) {
            reverseStack.pop();
        }
        part3Comp.clear();
        
    }
}
SatSolver::SatSolver(string filename){
    //processing input file
    std::ifstream fin(filename);
    if(!fin.is_open()){
        return;
    }
    int x, y;
    fin>> variables;
    fin >> clauses;
    while(fin>>x>>y){
        EdgeList.push_back({-x,y});
        EdgeList.push_back({-y,x});
    }
    fin.close();
    //initializing necessary objects
    lowlink =std::vector<int>(variables *2,0);
    index =std::vector<int>(variables*2,-1);
    isInStack =std::vector<bool>(variables*2,false);
    part3Comp = std::vector<int>();
    part2Sccs = std::vector<std::stack<int>>();
    part3Sccs = std::vector<std::vector<int>>();
    part3Values = std::vector<int>(variables,-1);
    part3SccValues = std::vector<int>(variables,-1);
}

void SatSolver::Part1(string filename){
    //create the Adj
    Build(variables, EdgeList);
    //Print the adj to the file
    PrintAdjList(filename,AdjL, variables);
}

void SatSolver::Part2(string filename){
    //open file
    ofstream fileOut(filename);
    if(!fileOut.is_open()){
        return;
    }
    SccStack = std::stack<int>();
    int nodeCount = 0;
    int nodeGo ;
    //process every vertex with Scc
    for(;nodeCount < variables*2;nodeCount++){
        if(nodeCount< variables){
            nodeGo = nodeCount-variables;//std::cout << nodeGoing<< endl  ;
        }else{
            nodeGo = nodeCount-variables+1;//std::cout << nodeGoing<< endl  ;
        }
        if(index[nodeCount] == -1){
            Scc(nodeGo);
        }
    }
    //Write the Sccs in a file
    for(std::stack<int> scc : part2Sccs){
        while(!scc.empty()){
            fileOut<<scc.top()<<" ";
            scc.pop();
        }
        fileOut<<endl;
    }
    fileOut.close();
}


void SatSolver::Part3(string filename){
    part3SccValues = std::vector<int>(part3Sccs.size(),-1);
    ofstream fileOut(filename);
    if(!fileOut.is_open()){
        return;
    }
    //check if the Scc components are valid
    for(auto &lists : part3Sccs){
        for(auto &nodes: lists){
            for(auto &nnodes : lists){
                if(-nodes == nnodes ){
                    fileOut<< -1 << endl ;
                    fileOut.close();
                    return ;
                }
            }
        }
    }
    //Give appropiate values to the variables
    for(int i =0 ; i <= part3SccValues.size()/2;i++){
        if(part3SccValues[i] == -1){
            part3SccValues[i] =0;
            for(int &node : part3Sccs[i]){
                if(node<0){
                    int temp = node*-1-1;
                    if(part3Values[temp] == -1){
                        part3Values[temp] = 0 ;
                    }
                }else{
                    if(part3Values[node-1] == -1){
                        part3Values[node-1] = 1 ;
                    }
                }
            }
        }
    }
    for(int &value :part3Values){
        fileOut<< value << " ";
    }
    fileOut << endl;
    fileOut.close();
}

